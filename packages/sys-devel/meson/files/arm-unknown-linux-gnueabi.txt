[binaries]
c = 'arm-unknown-linux-gnueabi-cc'
cpp = 'arm-unknown-linux-gnueabi-c++'
ar = 'arm-unknown-linux-gnueabi-ar'
strip = 'arm-unknown-linux-gnueabi-strip'
pkgconfig = 'arm-unknown-linux-gnueabi-pkg-config'

[host_machine]
system = 'linux'
cpu_family = 'arm'
cpu = 'arm'
endian = 'little'
